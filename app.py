from flask import Flask, url_for ,Blueprint

from werkzeug.serving import run_simple
from werkzeug.wsgi import DispatcherMiddleware


app = Flask(__name__)


bp = Blueprint('frontend', __name__, url_prefix='/sample_prefix')


 
#app.config["APPLICATION_ROOT"] = "/sample_prefix"
 
@bp.route("/")
def index():
    return "hello world"
    #return "The URL for this page is {}".format(url_for("index"))
 

@bp.route("/bye")
def bye():
    return "bye world"
    #return "The URL for this page is {}".format(url_for("index"))


app.register_blueprint(bp)


# def simple(env, resp):
#     resp(b'200 OK', [(b'Content-Type', b'text/plain')])
#     return [b"Hello WSGI World"]
 
#parent_app = DispatcherMiddleware(simple, {"/sample_prefix": app})
 
if __name__ == "__main__":
    app.run('0.0.0.0', 5000, app)
    #run_simple('0.0.0.0', 5000, parent_app)